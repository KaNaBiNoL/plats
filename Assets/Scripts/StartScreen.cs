﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class StartScreen : MonoBehaviour
{
    [SerializeField] GameObject gameScreen;
    [SerializeField] AudioMixer audioMixer;
    
    private void Start()
    {
        Time.timeScale = 0;
    }
    public void OnClickPlay()
    {
        gameScreen.gameObject.SetActive(true);
        gameObject.SetActive(false);
        Time.timeScale = 1;
        GameManager.Instantiate.CreateLevel();
    }

    public void OnClickQuit()
    {
        Application.Quit();
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }
}
