﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScreen : MonoBehaviour
{
    [SerializeField] GameObject startScreen;
    [SerializeField] GameObject gameScreen;

    public void OnClickResume()
    {
        gameScreen.gameObject.SetActive(true);
        gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void OnClickRestart()
    {
        GameManager.Instantiate.DestroyLevel();
        GameManager.Instantiate.CreateLevel();
        gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void OnClickMenu()
    {
        Time.timeScale = 0;
        GameManager.Instantiate.DestroyLevel();
        gameObject.SetActive(false);
        startScreen.SetActive(true);
        gameScreen.SetActive(false);
    }

    public void OnClickQuit()
    {
        Application.Quit();
    }
}
