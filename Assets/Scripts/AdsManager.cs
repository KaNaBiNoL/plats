﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        if (Advertisement.isSupported)
        {
            Advertisement.Initialize("3822265", true);
        }
    }

    public void PlayAds()
    {
        if (Advertisement.IsReady()) Advertisement.Show("video");
    }
}
