﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    [SerializeField] GameObject deathScreen;
    float record;
    Level level;

    public void Init(Level level)
    {
        this.level = level;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (level.FullScore > PlayerPrefs.GetFloat("Record", 0))
            {
                PlayerPrefs.SetFloat("Record", level.FullScore);
                GameManager.Instantiate.WriteNewRecordText();
            }
            record = PlayerPrefs.GetFloat("Record", level.FullScore);
            GameManager.Instantiate.SetCurrentRecordText(record.ToString("0"));
            Time.timeScale = 0;
            deathScreen.gameObject.SetActive(true);
            AdsManager.Instance.PlayAds();
        }
    }
}
