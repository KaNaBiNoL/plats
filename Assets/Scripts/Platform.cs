﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
     
    public int countToDeath;
    public bool jumpPermission;
    Level level;
    
    public void Init(Level level)
    {
        jumpPermission = true;
        this.level = level;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
        
        if (gameObject != null)
        {
            level.SetScore();
            GameManager.Instantiate.PlaySound();
            countToDeath++;
            if (countToDeath == 2)
            {
                Destroy(gameObject);
            }
        }

        if (rb != null)
        {
            if (countToDeath < 2)
            {
                if (jumpPermission)
                {
                    Vector2 velocity = rb.velocity;
                    velocity.y = Random.Range(4, 12);
                    rb.velocity = velocity;
                }
            }
        }

        if( collision.gameObject.CompareTag("Player"))
        {
            level.Score = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag ("Player"))
        {
            Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
            rb.constraints = RigidbodyConstraints2D.None;
            jumpPermission = false;
           if (level.GetSpawnPosPlatform().x > 0)
           {
               rb.AddForce(Vector2.left * 200f);
           }
           else
           {
               rb.AddForce(Vector2.right * 200f);
           }
        }
    }
}
