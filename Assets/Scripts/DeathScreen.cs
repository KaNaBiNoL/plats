﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScreen : MonoBehaviour
{
    [SerializeField] GameObject gameScreen;
    [SerializeField] GameObject startScreen;

    public void OnClickRestart()
    {
        GameManager.Instantiate.DestroyLevel();
        GameManager.Instantiate.CreateLevel();
        gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void OnClickMenu()
    {
        Time.timeScale = 0;
        gameScreen.SetActive(false);
        gameObject.SetActive(false);
        GameManager.Instantiate.DestroyLevel();
        startScreen.SetActive(true);
    }
    
}
