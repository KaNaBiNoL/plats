﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI fullScoreText;
    [SerializeField] TextMeshProUGUI jumpScoreText;
    [SerializeField] TextMeshProUGUI multiText;
    [SerializeField] TextMeshProUGUI currentRecordText;
    [SerializeField] TextMeshProUGUI newRecordText;
    [SerializeField] TextMeshProUGUI countToStartText;
    [SerializeField] Level levelPref;
    [SerializeField] StartScreen startScreen;
    [SerializeField] AudioClip jumpSound;
    AudioSource audioSource;
    Level level;
    public static GameManager Instantiate;
    int countToStart;

    private void Awake()
    {
        Instantiate = this;
    }

    private void Start()
    {
        startScreen.gameObject.SetActive(true);
        audioSource = Camera.main.GetComponent<AudioSource>();
    }
    
    public void SetFullScoreText(string text)
    {
        fullScoreText.text = text;
    }

    public void SetScoreText(string text)
    {
        jumpScoreText.text = text;
    }

    public void SetMultiText(string text)
    {
        multiText.text = text;
    }

    public void WriteNewRecordText()
    {
        newRecordText.text = "New Record!";
    }

    public void SetCurrentRecordText(string text)
    {
        currentRecordText.text = text;
    }

    public void CreateLevel()
    {
        level = Instantiate(levelPref);
        countToStart = 3;
        countToStartText.gameObject.SetActive(true);
        StartCoroutine(SetTextStartTime());       
    }

    public void DestroyLevel()
    {
        Level.Destroy(level.gameObject);
    }

    public void PlaySound()
    {
        audioSource.PlayOneShot(jumpSound);
    }

    IEnumerator SetTextStartTime()
    {
        while (countToStart > 0)
        {
            countToStartText.text = countToStart.ToString();
            yield return new WaitForSeconds(0.5f);
            countToStart--;
        }

        if (countToStart == 0)
        {
            countToStartText.text = "Go!";
            level.SetPlayerBodyType();
            yield return new WaitForSeconds(1f);
            countToStartText.gameObject.SetActive(false);
        }

    }

}

