﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    new Camera camera;
    [SerializeField] Transform camTarget;
    [SerializeField] Platform platPref;
    [SerializeField] GameObject player;
    [SerializeField] Vector2 spawnPosPlatform;
    [SerializeField] Vector3 camStartPos;
    PlayerDeath playerDeath;
    [SerializeField] float platformSpeed = 20f;
    Platform platform;
    float jumpScore;
    float multiScore;
    float fullScore;
    float posX = 5f;
    float offsetY = 0.5f;
    Vector2 platformFinishPos = Vector2.zero;

    private void Start()
    {
        camera = Camera.main;
        camera.transform.position = camStartPos;
        playerDeath = camera.GetComponentInChildren<PlayerDeath>();
        playerDeath.Init(this);
    }
    
    void Update()
    {
        if (Time.timeScale != 0 && player.GetComponent<Rigidbody2D>().velocity.y > 0)
        {
            jumpScore += player.GetComponent<Rigidbody2D>().velocity.y / 10;
        }
        GameManager.Instantiate.SetScoreText("jump:" + jumpScore.ToString("0"));
        GameManager.Instantiate.SetMultiText("x" + multiScore.ToString("0.0"));
        GameManager.Instantiate.SetFullScoreText("Score: " + fullScore.ToString("0"));

        if (Time.timeScale != 0) 
        { 
            if (player.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic && Input.GetMouseButtonDown(0))
            {
                SwitchSide();
                platform = Instantiate(platPref, spawnPosPlatform, Quaternion.identity, transform);
                platform.Init(this);
                multiScore += 0.5f;

            }

            if (platform != null)
            {
                platformFinishPos.y = platform.transform.position.y;
                platform.transform.position = Vector3.MoveTowards(platform.transform.position, platformFinishPos, Time.deltaTime * platformSpeed);
            }
        }
    }

    void LateUpdate()
    {
        if (camTarget.position.y > camera.transform.position.y)
        {
            camera.transform.position = new Vector3(camera.transform.position.x, camTarget.position.y, camera.transform.position.z);
        }
    }

    public float Score
    {
       set { jumpScore = value; }
    }

    public float FullScore
    {
        get { return fullScore; }
    }

    public void SwitchSide()
    {
        if (Random.value < 0.5)
        {
            spawnPosPlatform.x = posX;
        }
        else spawnPosPlatform.x = -posX;
        spawnPosPlatform.y += offsetY;
    }
  
    public Vector2 GetSpawnPosPlatform()
    {
        return spawnPosPlatform;
    }
    
    public void SetScore()
    {
        fullScore += multiScore * jumpScore;
        multiScore = 0;
    }

    public void SetPlayerBodyType()
    {
        player.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

}
